For a full introduction on PDDL, go
[here](https://github.com/nergmada/pddl-reference).

We will be specifically solving the
[tower of Hanoi](http://mathworld.wolfram.com/TowerofHanoi.html)
puzzle modeled in [PDDL](https://github.com/nergmada/pddl-reference/blob/master/docs/readme.md)
This animation is borrowed from Wolfram.com:

![Tower of Hanoi](hanoi.gif "Tower of Hanoi")

# Lay of the Land

- `cloud-solver` contains a fork of the original
[cloud-solver](https://bitbucket.org/planning-researchers/cloud-solver)
with added support for [popf](https://github.com/LCAS/popf)
and deployment as a [Docker container](cloud-solver/Dockerfile).

- `README.md` is this file

- `.gitlab-ci.yml` is the description of the CI/CD pipeline

# Running the planner from the .domains TLD (recommended)

## Development Environment

Go to this [on-line PDDL editor](http://editor.planning.domains/)
- Choose "Import PDDL File"
- Go to the "FF Benchmark Set"
- Choose "Hanoi", "pfile7.pddl"

- As we want to product a graph, we will have to use a custom planner:
`https://young-spire-39208.herokuapp.com`

## Graphical representation of... the graph

Copy-paste the graph produced previously as an **output** by the planner
(starts with `digraph Plan {`) and put it in a file
called e.g. `graph.dot`

[Graphviz](https://www.graphviz.org) will be able to turn
it into a PDF:
```bash
dot -Tpdf -O graph.dot
```
The PDF file will be named `graph.dot.pdf`.

Another way is to use an online Graphviz service such as
[this one](https://dreampuf.github.io/GraphvizOnline/)

# Running the planner locally (alternative)

Build the Docker image or fetch it from Gitlab:
```bash
docker build -t cloud-solver .
```

## Using the CLI

```bash
docker run --rm -v $PWD:/opt/webapp cloud-solver popf -B domain.pddl pfile2.pddl
```

## Using the web service

```bash
docker run --rm -p 127.0.0.1:5000:5000 --name cloud-solver cloud-solver
curl 'http://localhost:5000/solve?domain=http://www.haz.ca/planning-domains/classical/blocks/domain.pddl&problem=http://www.haz.ca/planning-domains/classical/blocks/probBLOCKS-4-1.pddl'
```

# Running the planner from its Heroku-hosted web service (alternative)

```bash
curl 'https://young-spire-39208.herokuapp.com/solve?domain=http://www.haz.ca/planning-domains/classical/blocks/domain.pddl&problem=http://www.haz.ca/planning-domains/classical/blocks/probBLOCKS-4-1.pddl'
```

# Hosting on Heroku

The custom on-line planner we are using is hosted as a Docker container
on Heroku, deployed following
[these instructions](https://devcenter.heroku.com/articles/container-registry-and-runtime).
To push a new image,
```bash
$ heroku container:push web
```
